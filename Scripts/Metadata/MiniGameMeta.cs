using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MiniGameMeta", menuName = "Config/MiniGameMeta")]
public class MiniGameMeta : ScriptableObject
{
    public int numberOfUsersAllowed;
    public float waitingTimeToStart;
    public string startGameMessage;


    public virtual void StartGame()
    {

    }

    public virtual void FinishGame()
    {

    }
}
