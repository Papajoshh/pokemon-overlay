using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MiniGameDungeon", menuName = "Config/MiniGameDungeonMeta")]

public class MiniGameDungeon : MiniGameMeta
{
    public string dungeonZone;
}
