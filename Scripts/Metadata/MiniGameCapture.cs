using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MiniGameCapture", menuName = "Config/MiniGameCaptureMeta")]

public class MiniGameCapture : MiniGameMeta
{
    public PokemonMeta[] availablePokemons;
    [SerializeField]
    GameObject pokemonController;

    PokemonController instantiatedPokemon;
    PokemonMeta pokemonToCatch;
    public override void StartGame()
    {
        base.StartGame();
        pokemonToCatch = availablePokemons[Random.Range(0, availablePokemons.Length)];

        instantiatedPokemon = Instantiate(pokemonController).GetComponent<PokemonController>();
        instantiatedPokemon.transform.SetParent(GameController.instance.characterContainer, false);
        instantiatedPokemon.transform.localPosition = new Vector3(Random.Range(-900, 0), -455.9f, 0);
        instantiatedPokemon.PreparePokemonController(pokemonToCatch, UIController.instance.mainCamera.ViewportToWorldPoint(Vector2.zero).x, UIController.instance.mainCamera.ViewportToWorldPoint(Vector2.one).x / 2);
        instantiatedPokemon.SpawnPokemon();

        GameController.instance.DespawnAllCharacters();

    }
}
