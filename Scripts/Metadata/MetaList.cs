using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MetaList", menuName = "Config/MetaList")]
public class MetaList : ScriptableObject
{
    public ScriptableObject[] metas;
}
