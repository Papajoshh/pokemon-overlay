using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PokemonMeta", menuName = "Config/PokemonMeta")]
public class PokemonMeta : ScriptableObject
{
    public int id;
    public string pokemonName;
    public int hpBase, attackBase, defenseBase, speedBase;
    public PokemonType[] types;
    public AnimationClip[] animations;

    Dictionary<AnimState, AnimationClip> animationClipByState = new Dictionary<AnimState, AnimationClip>();

    public void Initialize()
    {
        InitializeAnimations();
    }

    void InitializeAnimations()
    {
        animationClipByState.Clear();
        for (int index = 0; index < animations.Length; index++)
        {
            animationClipByState.Add(animations[index].animState, animations[index]);
        }
    }

    public AnimationClip GetAnimationClip(AnimState _animState)
    {
        return animationClipByState[_animState];
    }

    public int GetAttackBase()
    {
        return attackBase;
    }

    public int GetDefenseBase()
    {
        return defenseBase;
    }

    public int GetHPBase()
    {
        return hpBase;
    }

    public int GetSpeedBase()
    {
        return speedBase;
    }

    public int GetCurrentAttack()
    {
        return GetAttackBase();
    }

    public int GetCurrentDefense()
    {
        return GetDefenseBase();
    }

    public int GetCurrentHP()
    {
        return GetHPBase();
    }

    public int GetCurrentSpeed()
    {
        return GetSpeedBase();
    }
}

[System.Serializable]
public struct PokemonData
{
    public int pokemonIndex;
    public int level;
    public bool isUnlocked;
    public PokemonMeta pokemonConfig
    {
        get
        {
            if (m_pokemonConfig == null || m_pokemonConfig.id != pokemonIndex)
            {
                m_pokemonConfig = ((PokemonMeta)GeneralStats.pokemonList.metas[pokemonIndex]);
            }
            return m_pokemonConfig;
        }
    }
    PokemonMeta m_pokemonConfig;

    public PokemonData(PokemonMeta _meta)
    {
        pokemonIndex = _meta.id;
        level = 0;
        isUnlocked = false;
        m_pokemonConfig = _meta;
    }

    public int GetCurrentLevel()
    {
        return level;
    }
    public float GetCurrentLife()
    {
        return pokemonConfig.hpBase;
    }

    public float GetCurrentAttack()
    {
        return pokemonConfig.attackBase;
    }

    public float GetCurrentSpeed()
    {
        return pokemonConfig.speedBase;
    }

    public float GetCurrentDefense()
    {
        return pokemonConfig.defenseBase;
    }

    public PokemonType[] GetPokemonType()
    {
        return pokemonConfig.types;
    }
}

public enum PokemonType
{
    Normal = 0,
    Fire = 1,
    Water = 2,
    Grass = 3,
    Electric = 4,
    Ice = 5,
    Fighting = 6,
    Poison = 7,
    Ground = 8,
    Flying = 9,
    Psychic = 10,
    Bug = 11,
    Rock = 12,
    Ghost = 13,
    Dark = 14,
    Dragon = 15,
    Steel = 16,
    Fairy = 17,
}
