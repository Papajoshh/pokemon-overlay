﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitchLib.Client.Models;
using System.Linq;

[CreateAssetMenu(fileName ="userData", menuName = "Config/Create UserData")]

public class UsersData : ScriptableObject
{
    [SerializeField]
    PokemonData meta;
    public List<UserData> allUsers;
    public Dictionary<string, UserData> usersInfo = new Dictionary<string, UserData>();
    /// <summary>
    /// UserId / Username
    /// </summary>
    public Dictionary<string, string> userIdsOnChat = new Dictionary<string, string>();

    List<string> userIdToDelete = new List<string>();
    string userId;
    bool delete;
    UserData prevUserData;
    public void Initialize()
    {
        userIdToDelete.Clear();
        foreach (KeyValuePair<string, UserData> user in usersInfo)
        {
            delete = true;
            for (int index = 0; index < allUsers.Count; index++)
            {
                if (allUsers[index].userId == user.Key)
                {
                    delete = false;
                    continue;
                }
            }
            if (delete)
            {
                userIdToDelete.Add(user.Key);
            }
        }
        for (int index = 0; index < userIdToDelete.Count; index++)
        {
            usersInfo.Remove(userIdToDelete[index]);
        }
        for (int index = 0; index < allUsers.Count; index++)
        {
            if (allUsers[index].allPokemonData.allPokemons.Length < GeneralStats.pokemonList.metas.Length)
            {
                prevUserData = allUsers[index];
                allUsers[index] = new UserData
                {
                    username = prevUserData.username,
                    userId = prevUserData.userId,
                    isSub = prevUserData.isSub,
                    activePokemon = prevUserData.activePokemon,
                    allPokemonData = new PokemonSaveData
                    {
                        allPokemons = new PokemonData[GeneralStats.pokemonList.metas.Length]
                    }
                };

                for (int pokemonIndex = 0; pokemonIndex < prevUserData.allPokemonData.allPokemons.Length; pokemonIndex++)
                {
                    allUsers[index].allPokemonData.allPokemons[pokemonIndex] = prevUserData.allPokemonData.allPokemons[pokemonIndex];
                }
                for (int pokemonIndex = prevUserData.allPokemonData.allPokemons.Length; pokemonIndex < GeneralStats.pokemonList.metas.Length; pokemonIndex++)
                {
                    allUsers[index].allPokemonData.allPokemons[pokemonIndex] = new PokemonData((PokemonMeta)GeneralStats.pokemonList.metas[pokemonIndex]);
                }
            }
            usersInfo[allUsers[index].userId] = allUsers[index];
        }
        
    }

    public void AddUser(ChatMessage _userInfo)
    {
        AddUser(_userInfo.UserId, _userInfo.Username, _userInfo.IsSubscriber);
    }

    public UserData GetUserById(string _userId)
    {
        return usersInfo[_userId];
    }

    public UserData GetUserByUserName(string _userName)
    {
        return usersInfo[ userIdsOnChat[_userName]];
    }

    public void AddUser(string _userId, string _userName, bool _isSuscriber)
    {
        if (!usersInfo.ContainsKey(_userId))
        {
            UserData userData = new UserData
            {
                username = _userName,
                userId = _userId,
                isSub = _isSuscriber,
                activePokemon = new PokemonData
                {
                    isUnlocked = false,
                    level = -1,
                    pokemonIndex = -1
                },
                allPokemonData = new PokemonSaveData
                {
                    allPokemons = new PokemonData[GeneralStats.pokemonList.metas.Length]
                    
                }
            };
            for (int pokemonIndex = 0; pokemonIndex < userData.allPokemonData.allPokemons.Length; pokemonIndex++)
            {
                userData.allPokemonData.allPokemons[pokemonIndex] = new PokemonData((PokemonMeta)GeneralStats.pokemonList.metas[pokemonIndex]);
            }
            usersInfo.Add(_userId, userData);
            allUsers.Add(userData);
        }
        if (!userIdsOnChat.ContainsKey(_userId))
        {
            userIdsOnChat.Add(_userId, _userName);
            if (!usersInfo[_userId].HasActivePokemon())
            {

            }
            //usersInfo[_userId].ChangePokemon(meta);
        }
    }

    public bool IsUserStored(string _userId)
    {
        return usersInfo.ContainsKey(_userId);
    }

    public bool IsUserOnChat(string _userId)
    {
        return userIdsOnChat.ContainsKey(_userId);
    }

    public void ClearUser(string _username)
    {
        if (userIdsOnChat.ContainsValue(_username))
        {
            userId = userIdsOnChat.Single(x => x.Value == _username).Key;
            userIdsOnChat.Remove(userId);
        }
    }

    public void ClearUsersOnChat()
    {
        userIdsOnChat.Clear();
    }
}
[System.Serializable]
public class UserData
{
    public string username;
    public string userId;
    public bool isSub;
    public PokemonData activePokemon;
    public PokemonSaveData allPokemonData;

    public void ChangePokemon(PokemonData _pokemonMeta)
    {
        activePokemon = _pokemonMeta;
    }

    public void ChangePokemon(int _id)
    {
        if (!IsPokemonUnlocked(_id))
        {
            SetPokemonUnlocked(_id);
        }
        ChangePokemon(allPokemonData.allPokemons[_id]);
    }

    public void SetPokemonUnlocked(int _id)
    {
        allPokemonData.allPokemons[_id].isUnlocked = true;
        PokemonDatabase.instance.InsertNewPokemon(userId, allPokemonData.allPokemons[_id]);
    }

    public bool IsPokemonUnlocked(int _id)
    {
        return allPokemonData.allPokemons[_id].isUnlocked;
    }

    public bool HasActivePokemon()
    {
        return activePokemon.pokemonIndex >= 0;
    }

    public bool HaveAnyPokemon()
    {
        return allPokemonData.allPokemons.Count(x => x.isUnlocked) > 0;
    }
}
