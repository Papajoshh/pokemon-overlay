using UnityEngine;
using TwitchLib.Unity;

public class TwitchPubSub : MonoBehaviour
{
	private PubSub _pubSub;

	private void Start()
	{
		// To keep the Unity application active in the background, you can enable "Run In Background" in the player settings:
		// Unity Editor --> Edit --> Project Settings --> Player --> Resolution and Presentation --> Resolution --> Run In Background
		// This option seems to be enabled by default in more recent versions of Unity. An aditional, less recommended option is to set it in code:
		// Application.runInBackground = true;

		// Create new instance of PubSub Client
		_pubSub = new PubSub();

		// Subscribe to Events
		_pubSub.OnWhisper += OnWhisper;
		_pubSub.OnPubSubServiceConnected += OnPubSubServiceConnected;

		// Connect
		_pubSub.Connect();
	}

	private void OnPubSubServiceConnected(object sender, System.EventArgs e)
	{
		Debug.Log("PubSubServiceConnected!");

		// On connect listen to Bits evadsent
		// Please note that listening to the whisper events requires the chat_login scope in the OAuth token.
		_pubSub.ListenToWhispers(Secrets.client_id);

		// SendTopics accepts an oauth optionally, which is necessary for some topics, such as bit events.
		_pubSub.SendTopics(Secrets.bot_access_token);
	}

	private void OnWhisper(object sender, TwitchLib.PubSub.Events.OnWhisperArgs e)
	{
		Debug.Log($"{e.Whisper.Data}");
		// Do your bits logic here.
	}
}
