using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CommandManager : MonoBehaviour
{
    public static CommandManager instance;

    private void Awake()
    {
        instance = this;
    }
    public void DummyMethod(Command _command)
    {

    }

    public void SelectInitialPokemon(Command _command)
    {
        switch (_command.commandOrder)
        {
            case string pokemon when pokemon.ToLower() == "squirtle" || pokemon.ToLower() == "3":
                if (!_command.user.HaveAnyPokemon())
                {
                    GameController.instance.instantiatedCharacterControllerByUser[_command.user].ChangePokemon(6);
                }
                break;
            case string pokemon when pokemon.ToLower() == "bulbasaur" || pokemon.ToLower() == "1":
                if (!_command.user.HaveAnyPokemon())
                {
                    GameController.instance.instantiatedCharacterControllerByUser[_command.user].ChangePokemon(0);
                }
                break;
            case string pokemon when pokemon.ToLower() == "charmander" || pokemon.ToLower() == "2":
                if (!_command.user.HaveAnyPokemon())
                {
                    GameController.instance.instantiatedCharacterControllerByUser[_command.user].ChangePokemon(3);
                }
                break;
            case string pokemon when pokemon.ToLower() == "pikachu":
                if (!_command.user.HaveAnyPokemon())
                {
                    GameController.instance.instantiatedCharacterControllerByUser[_command.user].ChangePokemon(24);
                }
                break;
            case string pokemon when pokemon.ToLower() == "eevee":
                if (!_command.user.HaveAnyPokemon())
                {
                    GameController.instance.instantiatedCharacterControllerByUser[_command.user].ChangePokemon(132);
                }
                break;
            default:
                break;
        }
    }
    
    public void JoinGame(Command _command)
    {
        MinigameManager.instance.JoinUser(_command.user);
    }

    public void ExitGame(Command _command)
    {
        MinigameManager.instance.RemoveUserJoined(_command.user);
    }

    public CommandTypes GetCommandType(string _commandText)
    {
        switch (_commandText.ToLower())
        {
            case "select":
                return CommandTypes.SelectInitialPokemon;
            case "join":
                return CommandTypes.JoinGame;
            case "exit":
                return CommandTypes.ExitGame;
            default:
                return CommandTypes.None;
        }
    }

    public System.Action<Command> GetCommandCallback(Command _command)
    {
        switch (_command.commandType)
        {
            case CommandTypes.None:
                return DummyMethod;
            case CommandTypes.SelectInitialPokemon:
                return SelectInitialPokemon;
            case CommandTypes.JoinGame:
                return JoinGame;
            case CommandTypes.ExitGame:
                return ExitGame;
            default:
                return DummyMethod;
        }
    }

}

public enum CommandTypes
{
    None = -99,
    SelectInitialPokemon = 0,
    JoinGame = 1,
    ExitGame = 2

}
