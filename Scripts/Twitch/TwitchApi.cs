﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitchLib.Unity;

public class TwitchApi : MonoBehaviour
{
    public Api api;

    private void Start()
    {
        Application.runInBackground = true;
        api = new Api();
        api.Settings.AccessToken = Secrets.bot_access_token;
        api.Settings.ClientId = Secrets.client_id;

        //api.Invoke(api.Undocumented.GetChattersAsync(TwitchClient.instance.client.JoinedChannels[0].Channel), GetChatterListCallback);
    }


}
