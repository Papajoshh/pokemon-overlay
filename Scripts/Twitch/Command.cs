using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Command 
{
    public string commandOrder;
    public CommandTypes commandType;
    public UserData user;

    public Command(string _commandTitle, string _commandOrder, UserData _user)
    {
        commandOrder = _commandOrder;
        commandType = CommandManager.instance.GetCommandType(_commandTitle);
        user = _user;
    }

    public void RunCommand()
    {
        CommandManager.instance.GetCommandCallback(this).Invoke(this);
    }
}




