﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitchLib.Unity;
using TwitchLib.Client.Models;
using TwitchLib.Client.Events;
using TwitchLib;
using System;

public class TwitchClient : MonoBehaviour
{
    public Client client;
    public string channel_name = "Papajoshhh";

    public static TwitchClient instance;

    private UserData userData;
    private void Awake()
    {
        instance = this;    
    }

    private void Start()
    {
        Application.runInBackground = true;

        ConnectionCredentials credentials = new ConnectionCredentials("botjoshhh", Secrets.bot_access_token);
        client = new Client();
        client.Initialize(credentials, channel_name);

        client.OnChatCommandReceived += CommandReceived;
        client.OnMessageReceived += ChatMessageReceived;

        client.OnUserLeft += OnUserLeft;

        client.Connect();
    }

    private void OnUserLeft(object sender, OnUserLeftArgs e)
    {
        if (GeneralStats.usersData.IsUserOnChat(e.Username))
        {
            GameController.instance.RemoveUser(e.Username);
        }
    }

    private void ChatMessageReceived(object sender, OnMessageReceivedArgs e)
    {
        if (!GeneralStats.usersData.IsUserOnChat(e.ChatMessage.UserId))
        {
            GameController.instance.AddUser(e.ChatMessage);
        }
    }

    public void CommandReceived(object sender, OnChatCommandReceivedArgs e)
    {
        Command command = new Command(e.Command.CommandText, e.Command.ArgumentsAsString, GeneralStats.usersData.GetUserById(e.Command.ChatMessage.UserId));
        command.RunCommand();
    }

    public void SendMessageToTheChat(string _message)
    {
        client.SendMessage(channel_name, _message);
    }

}
