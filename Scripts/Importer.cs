using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
#if UNITY_EDITOR
public class MakeScriptableObject
{
    public static List<string> stringList = new List<string>();
    public static List<string[]> parsedList = new List<string[]>();

    public static PokemonMeta pokemon;

    static float rectX;
    static float rectY;
    static float prevRectY;
    static float rectW;
    static float prevRectH;
    static float rectH;
    static List<Sprite> spritesToAdd = new List<Sprite>();
    static int lastSpriteIndex;
    public static void CreateMyAsset(string _name)
    {
        PokemonMeta asset = ScriptableObject.CreateInstance<PokemonMeta>();

        AssetDatabase.CreateAsset(asset, "Assets/Meta/Pokemons/" + _name + ".asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }


    [MenuItem("Assets/Create/Refresh Pokemon Metas")]

    public static void readTextFile()
    {
        StreamReader inp_stm = new StreamReader("Assets/Resources/pokemonData.csv");

        while (!inp_stm.EndOfStream)
        {
            string inp_ln = inp_stm.ReadLine();

            stringList.Add(inp_ln);
        }

        inp_stm.Close();

        parseList();
    }

    static void parseList()
    {
        int pokemonIndex = 0;
        for (int i = 0; i < stringList.Count; i++)
        {
            string[] temp = stringList[i].Split(',');
            for (int j = 0; j < temp.Length; j++)
            {
                temp[j] = temp[j].Trim();  //removed the blank spaces
            }
            parsedList.Add(temp);
        }
        
        for (int index = 0; index < 600; index++)
        {
            
            if (!parsedList[index + 1][1].ToLower().Contains("mega "))
            {
                int.TryParse(parsedList[index + 1][0], out pokemonIndex);
                if (!File.Exists("Assets/Meta/Pokemons/" + pokemonIndex.ToString("D3") + "_" + parsedList[index + 1][1] + ".asset"))
                {
                    CreateMyAsset(pokemonIndex.ToString("D3") + "_" + parsedList[index + 1][1]);
                }
                pokemon = AssetDatabase.LoadAssetAtPath("Assets/Meta/Pokemons/" + pokemonIndex.ToString("D3") + "_" + parsedList[index + 1][1] + ".asset", typeof(PokemonMeta)) as PokemonMeta;

                pokemon.id = (pokemonIndex - 1);// parsedList[index + 1][0];
                                                //pokemon.pokemonName = parsedList[index + 1][1];
                                                //if (!string.IsNullOrEmpty(parsedList[index + 1][3]))
                                                //{
                                                //    pokemon.types = new PokemonType[2];
                                                //}
                                                //else
                                                //{
                                                //    pokemon.types = new PokemonType[1];
                                                //}
                                                //pokemon.types[0] = GetPokemonType(parsedList[index + 1][2]);
                                                //if (pokemon.types.Length > 1)
                                                //{
                                                //    pokemon.types[0] = GetPokemonType(parsedList[index + 1][3]);
                                                //}

                //pokemon.animations = new AnimationClip[2];


                //Object[]  spriteSheetObject = AssetDatabase.LoadAllAssetsAtPath("Assets/Sprites/Pokemon/" + parsedList[index + 1][1].ToUpper() + ".png") as Object[];
                //int auxiliarIndex = 0;
                //if (spriteSheetObject != null && spriteSheetObject.Length >= 1)
                //{
                //    Sprite[] spriteSheet;
                //    try
                //    {
                //        spriteSheet = new Sprite[spriteSheetObject.Length - 1];
                //    }
                //    catch (System.Exception)
                //    {
                //        Debug.Log("Ey");
                //        throw;
                //    }
                //    for (int spriteIndex = 0; spriteIndex < spriteSheetObject.Length; spriteIndex++)
                //    {
                //        if (spriteSheetObject[spriteIndex] is Sprite)
                //        {
                //            spriteSheet[auxiliarIndex] = spriteSheetObject[spriteIndex] as Sprite;
                //            auxiliarIndex++;
                //        }

                //    }
                //    //Idle
                //    bool breakLoop = false;
                //    pokemon.animations[0].animState = AnimState.Idle;
                //    prevRectY = spriteSheet[0].rect.y;
                //    prevRectH = spriteSheet[0].rect.height;
                //    spritesToAdd.Clear();
                //    for (int spriteSheetIndex = 0; spriteSheetIndex < spriteSheet.Length; spriteSheetIndex++)
                //    {
                //        rectX = spriteSheet[spriteSheetIndex].rect.x;
                //        rectY = spriteSheet[spriteSheetIndex].rect.y;
                //        rectW = spriteSheet[spriteSheetIndex].rect.width;
                //        rectH = spriteSheet[spriteSheetIndex].rect.height;
                //        if (rectY < prevRectY + prevRectH && rectY > prevRectY - prevRectH)
                //        {
                //            spritesToAdd.Add(spriteSheet[spriteSheetIndex]);
                //        }
                //        else
                //        {
                //            breakLoop = true;
                //        }
                //        prevRectY = rectY;
                //        prevRectH = rectH;
                //        if (breakLoop)
                //        {
                //            lastSpriteIndex = spriteSheetIndex;
                //            breakLoop = false;
                //            break;
                //        }
                //    }
                //    pokemon.animations[0].sprites = new Sprite[spritesToAdd.Count + 1];
                //    for (int spriteIndex = 0; spriteIndex < pokemon.animations[0].sprites.Length; spriteIndex++)
                //    {
                //        if (spriteIndex == pokemon.animations[0].sprites.Length - 1)
                //        {
                //            try
                //            {
                //                pokemon.animations[0].sprites[spriteIndex] = spritesToAdd[0];
                //            }
                //            catch (System.Exception)
                //            {
                //                Debug.Log("Ey");
                //                throw;
                //            }


                //        }
                //        else
                //        {
                //            pokemon.animations[0].sprites[spriteIndex] = spritesToAdd[spriteIndex];
                //        }

                //    }
                //    //Walking
                //    breakLoop = false;
                //    pokemon.animations[1].animState = AnimState.Walking;
                //    spritesToAdd.Clear();
                //    for (int spriteSheetIndex = lastSpriteIndex; spriteSheetIndex < spriteSheet.Length; spriteSheetIndex++)
                //    {
                //        rectX = spriteSheet[spriteSheetIndex].rect.x;
                //        rectY = spriteSheet[spriteSheetIndex].rect.y;
                //        rectW = spriteSheet[spriteSheetIndex].rect.width;
                //        rectH = spriteSheet[spriteSheetIndex].rect.height;
                //        if (rectY < prevRectY + prevRectH && rectY > prevRectY - prevRectH)
                //        {
                //            spritesToAdd.Add(spriteSheet[spriteSheetIndex]);
                //        }
                //        else
                //        {
                //            breakLoop = true;
                //        }
                //        prevRectY = rectY;
                //        prevRectH = rectH;
                //        if (breakLoop)
                //        {
                //            breakLoop = false;
                //            break;
                //        }
                //    }
                //    pokemon.animations[1].sprites = new Sprite[spritesToAdd.Count + 1];
                //    for (int spriteIndex = 0; spriteIndex < pokemon.animations[1].sprites.Length; spriteIndex++)
                //    {
                //        if (spriteIndex == pokemon.animations[1].sprites.Length - 1)
                //        {
                //            try
                //            {
                //                pokemon.animations[1].sprites[spriteIndex] = spritesToAdd[0];
                //            }
                //            catch (System.Exception)
                //            {
                //                Debug.Log("Ey");
                //                throw;
                //            }

                //        }
                //        else
                //        {
                //            pokemon.animations[1].sprites[spriteIndex] = spritesToAdd[spriteIndex];
                //        }

                //    }
                //}

                if (pokemon.id == 151)
                {
                    break;
                }
            }
            
        }
    }

    static PokemonType GetPokemonType(string _pokemonType)
    {
        string[] enumNames = System.Enum.GetNames(typeof(PokemonType));
        for (int index = 0; index < enumNames.Length; index++)
        {
            if (enumNames[index] == _pokemonType)
            {
                return (PokemonType)index;
            }
        }
        return PokemonType.Bug;
    }
}
#endif