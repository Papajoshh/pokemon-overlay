using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesReferences : MonoBehaviour
{
    public Sprite[] pokeballSprites;
    public static ResourcesReferences instance;
    public AnimationClip pokeball;
    private void Awake()
    {
        instance = this;
    }
}
