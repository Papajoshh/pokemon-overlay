using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : MonoBehaviour
{
    public static Director instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GeneralStats.Initialize();
        UIController.instance.Initialize();
        GameController.instance.InitializeGame(true);
    }
}
