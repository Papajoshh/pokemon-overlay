using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MetaList", menuName = "Config/MetaList")]
public static class GeneralStats
{
    public static MetaList pokemonList;
    public static UsersData usersData;
    public static void Initialize()
    {
        pokemonList = Resources.Load("Metas/PokemonList") as MetaList;
        for (int index = 0; index < pokemonList.metas.Length; index++)
        {
            ((PokemonMeta)pokemonList.metas[index]).Initialize();
        }
        usersData = Resources.Load("usersData") as UsersData;
        usersData.Initialize();
    }
}

[System.Serializable]
public struct TweenCurve
{
    public AnimationCurve transitionCurve;
    [HideInInspector]
    public float prog;
    public bool lerpOver1;
    public float preprog;
    public float animationDuration;
    public float delay;
    public bool unscaledTime;
    [HideInInspector]
    public float endValue;
    [HideInInspector]
    public float initialValue;
    [HideInInspector]
    public Vector3 endVectorValue;
    [HideInInspector]
    public Vector3 initialVectorValue;
    [HideInInspector]
    public Color endColorValue;
    [HideInInspector]
    public Color initialColorValue;
    [HideInInspector]
    public float floatValue;
    [HideInInspector]
    public Vector3 vectorValue;
    [HideInInspector]
    public Color colorValue;

    [SerializeField]
    public OptionalValues optionalValues;

    [System.Serializable]
    public struct OptionalValues
    {
        public RectTransform startPos;
        public RectTransform endPos;
        public RectTransform obj;
    }

    public void InitializeTween()
    {
        prog = -1;
        preprog = -1;
    }

    public void SetTweenParameters(float _initialValue, float _endValue)
    {
        initialValue = _initialValue;
        endValue = _endValue;
        prog = 0;

        if (delay > 0)
        {
            floatValue = _initialValue;
            preprog = 0;
        }

    }

    public void SetTweenParameters(float _initialValue, float _endValue, float _delay)
    {
        initialValue = _initialValue;
        endValue = _endValue;
        prog = 0;

        if (_delay > 0)
        {
            floatValue = _initialValue;
            delay = _delay;
            preprog = 0;
        }

    }

    public void SetTweenParameters(Vector3 _initialValue, Vector3 _endValue)
    {
        initialVectorValue = _initialValue;
        endVectorValue = _endValue;
        prog = 0;
        if (delay > 0)
        {
            preprog = 0;
        }
    }

    public void SetTweenParameters()
    {
        initialVectorValue = optionalValues.startPos.anchoredPosition;
        endVectorValue = optionalValues.endPos.anchoredPosition;
        prog = 0;
        if (delay > 0)
        {
            preprog = 0;
        }
    }

    public void SetTweenParameters(Color _initialValue, Color _endValue)
    {
        initialColorValue = _initialValue;
        endColorValue = _endValue;
        prog = 0;
        if (delay > 0)
        {
            preprog = 0;
        }
    }

    public void TweenFloat()
    {
        if (CanRunTween())
        {
            if (delay > 0 && preprog >= 0)
            {
                preprog = Mathf.Clamp01(preprog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / delay);
            }
            if (preprog >= 1 || preprog < 0)
            {
                if (lerpOver1)
                {
                    prog = prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration;
                }
                else
                {
                    prog = Mathf.Clamp01(prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration);
                }

                floatValue = Mathf.LerpUnclamped(initialValue, endValue, transitionCurve.Evaluate(prog));
                if (prog >= 1)
                {
                    prog = -1;
                }
            }
        }
    }

    public void StopTween()
    {
        prog = -1;
        preprog = -1;
    }

    public void ForwardToEnd()
    {
        prog = 1;
        preprog = 1;
    }

    public void Prepare()
    {
        vectorValue = Vector3.LerpUnclamped(initialVectorValue, endVectorValue, transitionCurve.Evaluate(prog));

        if (optionalValues.obj != null)
        {
            optionalValues.obj.anchoredPosition = vectorValue;
        }
    }

    public void TweenVector()
    {
        if (CanRunTween())
        {
            if (delay > 0 && preprog >= 0)
            {
                preprog = Mathf.Clamp01(preprog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / delay);
            }
            if (preprog >= 1 || preprog < 0)
            {
                if (lerpOver1)
                {
                    prog = prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration;
                }
                else
                {
                    prog = Mathf.Clamp01(prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration);
                }
                vectorValue = Vector3.LerpUnclamped(initialVectorValue, endVectorValue, transitionCurve.Evaluate(prog));

                if (optionalValues.obj != null)
                {
                    optionalValues.obj.anchoredPosition = vectorValue;
                }

                if (prog >= 1)
                {
                    prog = -1;
                }
            }

        }
    }

    public void TweenColor()
    {
        if (CanRunTween())
        {
            if (delay > 0 && preprog >= 0)
            {
                preprog = Mathf.Clamp01(preprog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / delay);
            }
            if (preprog >= 1 || preprog < 0)
            {
                if (lerpOver1)
                {
                    prog = prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration;
                }
                else
                {
                    prog = Mathf.Clamp01(prog + (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime) / animationDuration);
                }
                colorValue = Color.Lerp(initialColorValue, endColorValue, transitionCurve.Evaluate(prog));
                if (prog >= 1)
                {
                    prog = -1;
                }
            }
        }
    }

    public float GetTweenValue()
    {
        return transitionCurve.Evaluate(prog);
    }

    public bool CanRunTween()
    {
        return prog >= 0;
    }

    public bool IsTweenFinished()
    {
        return prog >= 1 || prog <= -1;
    }

    //Aunque sea float al forzar el 0 se puede usar como referencia
    public bool IsTweenStarting()
    {
        return prog == 0;
    }

    public bool IsDelayOver()
    {
        return (delay > 0 && preprog >= 1) || delay <= 0;
    }
}

