using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    public static MinigameManager instance;

    public MiniGameMeta currentMinigame;

    public bool isAnyGameActive
    {
        get
        {
            return currentMinigame != null;
        }
    }
    public List<UserData> usersJoined = new List<UserData>();

    private void Awake()
    {
        instance = this;
    }

    public void JoinUser(UserData _user)
    {
        if (isAnyGameActive && !usersJoined.Contains(_user) && usersJoined.Count < currentMinigame.numberOfUsersAllowed)
        {
            usersJoined.Add(_user);
            TwitchClient.instance.SendMessageToTheChat(_user.username + " se ha unido a la party");
        }
        else
        {
            if (usersJoined.Contains(_user))
            {
                TwitchClient.instance.SendMessage(_user.username + ", ya est�s en la party");
            }
            else if (usersJoined.Count >= currentMinigame.numberOfUsersAllowed)
            {
                TwitchClient.instance.SendMessageToTheChat("L�mite m�ximo alcanzado, lo siento :/");
            }
        }
    }

    public void RemoveUserJoined(UserData _user)
    {
        if (isAnyGameActive && usersJoined.Contains(_user))
        {
            usersJoined.Remove(_user);
            TwitchClient.instance.SendMessageToTheChat(_user.username + " ha dejado la party");
        }
    }

    public void StartGame(MiniGameMeta _miniGameToStart)
    {
        currentMinigame = _miniGameToStart;
        TwitchClient.instance.SendMessageToTheChat(currentMinigame.startGameMessage);
    }

    public void FinishGame()
    {
        currentMinigame = null;

    }
}
