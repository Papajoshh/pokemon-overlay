using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class GameEditor : MonoBehaviour
{
    [SerializeField]
    CharacterController characterControllerPrefab;
    [SerializeField]
    Transform characterContainer;

    List<CharacterController> instantiatedCharacterControllers = new List<CharacterController>();
    public void CheckAnimations()
    {
        if (instantiatedCharacterControllers.Count <= 0)
        {
            for (int index = 0; index < 25; index++)
            {
                CharacterController instantiatedCharacterController = Instantiate(characterControllerPrefab).GetComponent<CharacterController>();
                instantiatedCharacterControllers.Add(instantiatedCharacterController);
                instantiatedCharacterController.transform.SetParent(characterContainer, false);
                instantiatedCharacterController.transform.localPosition = new Vector3(Random.Range(-900, 900), -455.9f, 0);
                instantiatedCharacterController.PrepareCharacterController(GeneralStats.usersData.usersInfo["papajoshhh"]);
            }
            
        }
        
    }

    public void SelectPokemon()
    {
        GameController.instance.instantiatedCharacterControllerByUser.First().Value.ChangePokemon(1);
    }

    public void SelectPokemon(int _id)
    {
        GameController.instance.instantiatedCharacterControllerByUser.First().Value.ChangePokemon(_id);
    }

    public void RefreshPokemonOwned()
    {
        PokemonDatabase.instance.InsertAllPokemonOwned();
    }
}
