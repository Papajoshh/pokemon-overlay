using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonController : MonoBehaviour
{
    [SerializeField]
    CharacterAnimation characterAnimation;
    [SerializeField]
    float walkingSpeed;
    [SerializeField]
    TweenCurve spawnPokemon;

    public BoxCollider2D collider;
    PokemonMeta storedMeta;
    AnimState currentAnimState;
    MoveType moveType;
    float startTimeAnimation;
    float endTimeAnimation;

    [SerializeField]
    float minTimeIdle, maxTimeIdle;
    [SerializeField]
    float minTimeWalking, maxTimeWalking;
    Color initialColor;
    Color endColor;
    int goalPokemon;

    float xMinAllowed;
    float xMaxAllowed;
    bool canWalk;
    private void Awake()
    {
        InitializeTweens();
    }

    void InitializeTweens()
    {
        spawnPokemon.InitializeTween();
    }

    public void PreparePokemonController(PokemonMeta _pokemonConfig, float _xMin, float _xMax)
    {
        storedMeta = _pokemonConfig;
        canWalk = false;
    }

    public void SetWalkState(bool _state)
    {
        canWalk = _state;
    }

    public void SpawnPokemon()
    {
        spawnPokemon.SetTweenParameters(characterAnimation.spriteImage.color, Color.white);
        RefreshPokemonVisual();
        characterAnimation.PauseAnimation();
    }

    void RefreshPokemonVisual()
    {
        characterAnimation.spriteImage.transform.eulerAngles = Vector3.zero;
        characterAnimation.StartAnimation(storedMeta.GetAnimationClip(AnimState.Idle));
        currentAnimState = AnimState.Idle;
        endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
    }


    private void Update()
    { 


        if (Time.time >= endTimeAnimation)
        {
            switch (currentAnimState)
            {
                case AnimState.Idle:
                    if (canWalk)
                    {
                        currentAnimState = AnimState.Walking;
                        characterAnimation.StartAnimation(storedMeta.GetAnimationClip(AnimState.Walking));
                        endTimeAnimation = Time.time + Random.Range(minTimeWalking, maxTimeWalking);
                        moveType = (MoveType)Random.Range(0, 2);
                        if (moveType == MoveType.Left)
                        {
                            characterAnimation.FlipSprite();
                        }
                        else
                        {
                            characterAnimation.SetDefaultRotation();
                        }
                    }
                    break;
                case AnimState.Walking:
                    currentAnimState = AnimState.Idle;
                    characterAnimation.StartAnimation(storedMeta.GetAnimationClip(AnimState.Idle));
                    endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    break;
                case AnimState.PokeballRumble:

                    break;
                default:
                    break;
            }

        }
        switch (currentAnimState)
        {
            case AnimState.Idle:
                break;
            case AnimState.Walking:
                if (moveType == MoveType.Left)
                {
                    transform.Translate(Vector3.left * walkingSpeed * Time.deltaTime);
                    if (transform.position.x <= xMinAllowed)
                    {
                        currentAnimState = AnimState.Idle;
                        characterAnimation.StartAnimation(storedMeta.GetAnimationClip(AnimState.Idle));
                        endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    }
                }
                else
                {
                    transform.Translate(Vector3.right * walkingSpeed * Time.deltaTime);
                    if (transform.position.x >= xMaxAllowed)
                    {
                        currentAnimState = AnimState.Idle;
                        characterAnimation.StartAnimation(storedMeta.GetAnimationClip(AnimState.Idle));
                        endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    }
                }
                break;
            default:
                break;
        }

        if (spawnPokemon.CanRunTween())
        {
            spawnPokemon.TweenColor();
            characterAnimation.spriteImage.color = spawnPokemon.colorValue;

            if (spawnPokemon.IsTweenFinished())
            {
                characterAnimation.ResumeAnimation();
            }
        }
    }

}
