using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitchLib.Client.Models;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    UsersData usersData
    {
        get
        {
            if (m_usersData == null)
            {
                m_usersData = GeneralStats.usersData;
            }
            return m_usersData;
        }
    }
    UsersData m_usersData;
    [SerializeField]
    CharacterController characterControllerPrefab;
    public Transform characterContainer;
    [SerializeField]
    bool blockChatUsers;

    public Dictionary<UserData, CharacterController> instantiatedCharacterControllerByUser = new Dictionary<UserData, CharacterController>();
    public Dictionary<Collider2D, CharacterController> instantiatedCharacterControllerByTextCollider = new Dictionary<Collider2D, CharacterController>();
    private void Awake()
    {
        instance = this;

    }

    private void Update()
    {
        CheckTextColliders();
    }

    public void AddUser(ChatMessage _chatMessage)
    {
        if (!blockChatUsers && !usersData.IsUserOnChat(_chatMessage.UserId))
        {
            usersData.AddUser(_chatMessage);
            CharacterController instantiatedCharacterController = Instantiate(characterControllerPrefab).GetComponent<CharacterController>();
            instantiatedCharacterControllerByUser.Add(usersData.usersInfo[_chatMessage.UserId], instantiatedCharacterController);
            instantiatedCharacterControllerByTextCollider.Add(instantiatedCharacterController.collider, instantiatedCharacterController);
            instantiatedCharacterController.transform.SetParent(characterContainer, false);
            instantiatedCharacterController.transform.localPosition = new Vector3(Random.Range(-900, 900), -455.9f, 0);
            instantiatedCharacterController.PrepareCharacterController(usersData.usersInfo[_chatMessage.UserId]);
        }
        
    }

    public void DespawnAllCharacters()
    {
        foreach (KeyValuePair<UserData, CharacterController> character in instantiatedCharacterControllerByUser)
        {
            character.Value.DespawnCharacter();
        }
    }

    public UserData GetUserData(string _userName)
    {
        return usersData.GetUserByUserName(_userName);
    }

    public void SpawnBot()
    {
        string userId = Random.Range(0, 1000).ToString();
        string userName = "bot" + Random.Range(0, 1000).ToString();
        if (!usersData.IsUserOnChat(userId))
        {
            usersData.AddUser(userId, userName, false);
            usersData.usersInfo[userId] = new UserData
            {
                username = userName,
                userId = userId,
                isSub = false,
                activePokemon = new PokemonData
                {
                    isUnlocked = false,
                    pokemonIndex = -1,
                    level = -1
                },
                allPokemonData = new PokemonSaveData
                {
                    allPokemons = new PokemonData[GeneralStats.pokemonList.metas.Length]
                }
            };
            for (int pokemonIndex = 0; pokemonIndex < GeneralStats.pokemonList.metas.Length; pokemonIndex++)
            {
                usersData.usersInfo[userId].allPokemonData.allPokemons[pokemonIndex] = new PokemonData((PokemonMeta)GeneralStats.pokemonList.metas[pokemonIndex]);
            }
            //usersData.usersInfo[userId].ChangePokemon(Random.Range(0, GeneralStats.pokemonList.metas.Length));
            
            CharacterController instantiatedCharacterController = Instantiate(characterControllerPrefab).GetComponent<CharacterController>();
            instantiatedCharacterControllerByUser.Add(usersData.usersInfo[userId], instantiatedCharacterController);
            instantiatedCharacterControllerByTextCollider.Add(instantiatedCharacterController.collider, instantiatedCharacterController);
            instantiatedCharacterController.transform.SetParent(characterContainer, false);
            instantiatedCharacterController.transform.localPosition = new Vector3(Random.Range(-900, 900), -446.5f, 0);
            instantiatedCharacterController.PrepareCharacterController(usersData.usersInfo[userId]);
        }
    }

    public void RemoveUser(string _userName)
    {
        usersData.ClearUser(_userName);
    }

    void CheckTextColliders()
    {
        foreach (KeyValuePair<UserData, CharacterController> instantiatedCharacterController in instantiatedCharacterControllerByUser)
        {
            instantiatedCharacterController.Value.canCheckText = true;
        }

        foreach (KeyValuePair<UserData, CharacterController> instantiatedCharacterController in instantiatedCharacterControllerByUser)
        {
            if (instantiatedCharacterController.Value.canCheckText)
            {
                instantiatedCharacterController.Value.CheckTextCollision();
            }
            
        }
    }

    

    public void InitializeGame(bool _clearUsers)
    {
        if (_clearUsers)
        {
            instantiatedCharacterControllerByUser.Clear();
            instantiatedCharacterControllerByTextCollider.Clear();
            usersData.ClearUsersOnChat();
        }
    }

    
}
