using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    CharacterAnimation characterAnimation;
    [SerializeField]
    PokemonMeta meta;
    [SerializeField]
    Text viewerNameText;
    [SerializeField]
    RectTransform viewerNameRect;
    [SerializeField]
    float walkingSpeed;
    [SerializeField]
    GameObject pokebalParticlesSystem;
    [SerializeField]
    TweenCurve nameTransition, pokeballRumbleAnimation, pokeballClose, spawnPokemon;

    public BoxCollider2D collider;
    [SerializeField]
    float heightCell;
    [SerializeField]
    Vector2 sizeCell;

    UserData storedUser;
    PokemonData storedMeta;
    AnimState currentAnimState;
    MoveType moveType;

    float startTimeAnimation;
    float endTimeAnimation;

    [SerializeField]
    float minTimeIdle, maxTimeIdle;
    [SerializeField]
    float minTimeWalking, maxTimeWalking;

    bool resizeCollider;
    bool isChangingPokemon;
    public bool canCheckText;
    Vector2 defaultTextPosition;
    Vector2 defaultTextanchoredPosition;
    Collider2D[] allCollidersInBoxPosition;
    Collider2D[] allCollidersInColumn;
    Color initialColor;
    Color endColor;
    CharacterController auxiliarController;
    List<int> freePositionsIndex = new List<int>();
    public int currentIndex = 0;
    int prevIndex = 0;
    int indexToSet;
    int debugCount;
    int goalPokemon;
    static readonly int levelOfShow = 4;
    RaycastHit m_Hit;
    float canvasPixelScaler
    {
        get
        {
            return UIController.instance.canvasScaler.referencePixelsPerUnit;
        }
    }

    private void Awake()
    {
        InitializeTweens();
    }

    void InitializeTweens()
    {
        nameTransition.InitializeTween();
        pokeballRumbleAnimation.InitializeTween();
        pokeballClose.InitializeTween();
        spawnPokemon.InitializeTween();
    }

    public void PrepareCharacterController(UserData _userData)
    {
        storedUser = _userData;
        storedMeta = _userData.activePokemon;

        RefreshPokemonVisual();


        viewerNameText.text = _userData.username;
        
        defaultTextPosition = viewerNameRect.position;
        defaultTextanchoredPosition = viewerNameRect.anchoredPosition;
        resizeCollider = true;
    }

    void RefreshPokemonVisual()
    {
        if (storedUser.HasActivePokemon())
        {
            characterAnimation.spriteImage.transform.eulerAngles = Vector3.zero;
            meta = storedUser.activePokemon.pokemonConfig;
            characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Idle));
            currentAnimState = AnimState.Idle;
            endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
        }
        else
        {
            characterAnimation.spriteImage.sprite = ResourcesReferences.instance.pokeballSprites[0];
            currentAnimState = AnimState.PokeballRumble;
            pokeballRumbleAnimation.SetTweenParameters(-8, 8);
        }
    }

    public void DespawnCharacter()
    {
        spawnPokemon.SetTweenParameters(characterAnimation.spriteImage.color, Color.clear);
        transform.position = new Vector3(20, 20, 20);
    }

    private void Update()
    {
        if (resizeCollider)
        {
            collider.size = viewerNameRect.sizeDelta;
            resizeCollider = false;
        }

        if (nameTransition.CanRunTween())
        {
            nameTransition.TweenVector();
            viewerNameRect.transform.position = new Vector3(viewerNameRect.transform.position.x, nameTransition.vectorValue.y, viewerNameRect.transform.position.z);
            if ((currentIndex >= levelOfShow && viewerNameText.color.a > 0) || (currentIndex <= levelOfShow && viewerNameText.color.a < 1))
            {
                viewerNameText.color = Color.Lerp(initialColor, endColor, ((nameTransition.prog < 0) ? 1 : nameTransition.prog));
            }
        }

        if (Time.time >= endTimeAnimation && !isChangingPokemon)
        {
            switch (currentAnimState)
            {
                case AnimState.Idle:
                    currentAnimState = AnimState.Walking;
                    characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Walking));
                    endTimeAnimation = Time.time + Random.Range(minTimeWalking, maxTimeWalking);
                    moveType = (MoveType)Random.Range(0, 2);
                    if (moveType == MoveType.Left)
                    {
                        characterAnimation.FlipSprite();
                    }
                    else
                    {
                        characterAnimation.SetDefaultRotation();
                    }
                    break;
                case AnimState.Walking:
                    currentAnimState = AnimState.Idle;
                    characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Idle));
                    endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    break;
                case AnimState.PokeballRumble:

                    break;
                default:
                    break;
            }
            
        }
        switch (currentAnimState)
        {
            case AnimState.Idle:
                break;
            case AnimState.Walking:
                if (moveType == MoveType.Left)
                {
                    transform.Translate(Vector3.left * walkingSpeed * Time.deltaTime);
                    if (UIController.instance.mainCamera.WorldToViewportPoint(transform.position).x <= 0.03f)
                    {
                        currentAnimState = AnimState.Idle;
                        characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Idle));
                        endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    }
                }
                else
                {
                    transform.Translate(Vector3.right * walkingSpeed * Time.deltaTime);
                    if (UIController.instance.mainCamera.WorldToViewportPoint(transform.position).x >= 0.97f)
                    {
                        currentAnimState = AnimState.Idle;
                        characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Idle));
                        endTimeAnimation = Time.time + Random.Range(minTimeIdle, maxTimeIdle);
                    }
                }
                break;
            case AnimState.PokeballRumble:
                if (pokeballRumbleAnimation.CanRunTween())
                {
                    pokeballRumbleAnimation.TweenFloat();
                    if (pokeballRumbleAnimation.IsDelayOver())
                    {
                        characterAnimation.spriteImage.transform.eulerAngles = new Vector3(characterAnimation.transform.eulerAngles.x, characterAnimation.transform.eulerAngles.y, pokeballRumbleAnimation.floatValue);
                        if (pokeballRumbleAnimation.IsTweenFinished())
                        {
                            pokeballRumbleAnimation.prog = 0;
                            pokeballRumbleAnimation.preprog = 0;
                            pokeballRumbleAnimation.delay = Random.Range(0.5f, 2f);
                        }
                    }
                    
                }
                break;
            case AnimState.PokeballOpen:
                if (characterAnimation.animationEnded)
                {
                    characterAnimation.ResetAnimationState();
                    pokeballClose.SetTweenParameters(characterAnimation.spriteImage.color, Color.clear);
                    currentAnimState = AnimState.Idle;
                }
                break;
            default:
                break;
        }

        if (pokeballClose.CanRunTween())
        {
            pokeballClose.TweenColor();
            characterAnimation.spriteImage.color = pokeballClose.colorValue;
            if (pokeballClose.IsTweenFinished())
            {
                spawnPokemon.SetTweenParameters(characterAnimation.spriteImage.color, Color.white);
                storedUser.ChangePokemon(goalPokemon);
                RefreshPokemonVisual();
                characterAnimation.PauseAnimation();
            }
        }

        if (spawnPokemon.CanRunTween())
        {
            spawnPokemon.TweenColor();
            characterAnimation.spriteImage.color = spawnPokemon.colorValue;
            
            if (spawnPokemon.IsTweenFinished())
            {
                isChangingPokemon = false;
                characterAnimation.ResumeAnimation();
            }
        }
    }
    

    public void CheckTextCollision()
    {
        for (int index = 0; index < levelOfShow + 1; index++)
        {
            allCollidersInBoxPosition = Physics2D.OverlapBoxAll(new Vector2(viewerNameRect.position.x, defaultTextPosition.y) + Vector2.up * index * collider.size.y / canvasPixelScaler, collider.size / canvasPixelScaler / 2, 0);
            //Hay un texto en esta posici�n
            if (allCollidersInBoxPosition.Length >= 1 + ((currentIndex == index) ? 1 : 0))
            {

            }
            //S�lo est� el propio texto
            else if (allCollidersInBoxPosition.Length >= 0 + ((currentIndex == index)?1:0))
            {
                prevIndex = currentIndex;
                indexToSet = index;
                allCollidersInColumn = GetCollidersInThisColumn();
                CheckAndSetIndex(index);
                currentIndex = indexToSet;
                if (prevIndex != currentIndex)
                {
                    nameTransition.SetTweenParameters(viewerNameRect.transform.position, new Vector2(viewerNameRect.position.x, defaultTextPosition.y) + Vector2.up * currentIndex * (collider.size.y / canvasPixelScaler));
                }
                initialColor = viewerNameText.color;
                if (currentIndex >= levelOfShow)
                {
                    endColor = Color.clear;
                }
                else
                {
                    endColor = Color.white;
                }
                //SetViewerNamePosition(new Vector2(viewerNameRect.position.x, defaultTextPosition.y) + Vector2.up * currentIndex * (collider.size.y / canvasPixelScaler));//defaultTextanchoredPosition + Vector2.up * index * (viewerNameRect.sizeDelta.y));
                break;
            }
        }
    }

    void CheckAndSetIndex(int _initialIndex)
    {
        freePositionsIndex.Clear();
        for (int index = 0; index < levelOfShow + 1; index++)
        {
            freePositionsIndex.Add(index);
        }
        
        for (int collidersIndex = 0; collidersIndex < allCollidersInColumn.Length; collidersIndex++)
        {
            if (allCollidersInColumn[collidersIndex] != collider && freePositionsIndex.Contains(GameController.instance.instantiatedCharacterControllerByTextCollider[allCollidersInColumn[collidersIndex]].currentIndex))
            {
                freePositionsIndex.Remove(GameController.instance.instantiatedCharacterControllerByTextCollider[allCollidersInColumn[collidersIndex]].currentIndex);
            }
        }
        for (int index = 0; index < levelOfShow + 1; index++)
        {
            if (freePositionsIndex.Contains(index))
            {
                indexToSet = index;
                break;
            }
        }
    }

    public void SetViewerNamePosition(Vector2 _anchoredPosition)
    {
        viewerNameRect.transform.position = _anchoredPosition;// = _anchoredPosition;
    }

    public Collider2D[] GetCollidersInThisColumn()
    {
        return Physics2D.OverlapBoxAll(new Vector2(viewerNameRect.position.x, defaultTextPosition.y), new Vector2(collider.size.x / canvasPixelScaler, collider.size.y / canvasPixelScaler * (levelOfShow + 1) * 2) /2, 0);
    }

    public void StarWalking()
    {
        characterAnimation.StartAnimation(meta.GetAnimationClip(AnimState.Walking));
    }

    public bool IsCharacterInThisState(AnimState _anim)
    {
        return currentAnimState == _anim;
    }

    public void ChangePokemon(int _id)
    {
        if (!isChangingPokemon)
        {
            goalPokemon = _id;
            isChangingPokemon = true;
            meta = storedUser.allPokemonData.allPokemons[_id].pokemonConfig;
            currentAnimState = AnimState.PokeballOpen;
            characterAnimation.StartAnimation(ResourcesReferences.instance.pokeball);
            GameObject instantiatedParticles = Instantiate(pokebalParticlesSystem);
            instantiatedParticles.transform.SetParent(transform, false);
            instantiatedParticles.transform.localPosition = Vector3.zero;
            instantiatedParticles.transform.localScale = 250 * Vector3.one;
        }
        
    }

    
}


public enum AnimState
{
    Idle,
    Walking,
    PokeballRumble,
    PokeballOpen
}

public enum MoveType
{
    Left,
    Right
}
