using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Camera mainCamera;
    public CanvasScaler canvasScaler;

    public static UIController instance;

    private void Awake()
    {
        instance = this;
    }

    public void Initialize()
    {
        mainCamera = Camera.main;
    }
}
