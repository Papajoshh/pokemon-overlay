using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterAnimation : MonoBehaviour
{
    public Image spriteImage;

    float prog = -1;
    AnimationClip actualAnimationClip;
    [HideInInspector]
    public bool animationEnded;

    bool pauseAnimation;
    private void Update()
    {
        if (prog >= 0 && !pauseAnimation)
        {
            prog = Mathf.Clamp01(prog + Time.deltaTime / actualAnimationClip.timeBetweenSprites);
            if (prog >= 1)
            {
                
                if (!actualAnimationClip.IsLoopFinished())
                {
                    spriteImage.sprite = actualAnimationClip.GetNextSprite();
                    prog = 0;
                }
                else
                {
                    if (actualAnimationClip.loop)
                    {
                        spriteImage.sprite = actualAnimationClip.GetNextSprite();
                        prog = 0;
                    }
                    else
                    {
                        animationEnded = true;
                    }
                }
                
                
            }
        }
    }

    public void PauseAnimation()
    {
        pauseAnimation = true;
    }

    public void ResumeAnimation()
    {
        pauseAnimation = false;
    }
    public void StartAnimation(AnimationClip _animationToRun)
    {
        animationEnded = false;
        actualAnimationClip = _animationToRun;
        spriteImage.sprite = actualAnimationClip.sprites[0];
        prog = 0;
    }

    public void StopAnimation()
    {
        prog = -1;
    }

    public void ResetAnimationState()
    {
        animationEnded = false;
    }

    public void FlipSprite()
    {
        transform.eulerAngles = new Vector3(0, 180, 0);
    }
    public void SetDefaultRotation()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }

}

[System.Serializable]
public struct AnimationClip
{
    public AnimState animState;
    public int index;
    public Sprite[] sprites;
    public float timeBetweenSprites;
    [HideInInspector]
    public int actualSpriteIndex;
    public bool loop;
    
    public void InitializeAnimation()
    {
        actualSpriteIndex = 0;
    }

    public Sprite GetNextSprite()
    {
        actualSpriteIndex++;
        actualSpriteIndex = actualSpriteIndex % sprites.Length;
        return sprites[actualSpriteIndex];
    }

    public bool IsLoopFinished()
    {
        return actualSpriteIndex >= sprites.Length - 1;
    }
}
