using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MongoDB.Driver;
using MongoDB.Bson;

public class PokemonDatabase : MonoBehaviour
{
    MongoClient client = new MongoClient(DatabaseSecrets.clientData);
    IMongoDatabase database;
    IMongoCollection<BsonDocument> pokemonListCollection;
    IMongoCollection<BsonDocument> pokemonOwnedCollection;

    public static PokemonDatabase instance;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        database = client.GetDatabase("Mongus");
        pokemonListCollection = database.GetCollection<BsonDocument>("Pokamol_List");
        pokemonOwnedCollection = database.GetCollection<BsonDocument>("Pokamol_Owned");
    }

    public void DeleteAllPokemonsAsync()
    {
        Database.DeleteAllAsync(pokemonListCollection);
    }

    public void DeleteAllPokemonOwned()
    {
        Database.DeleteAllAsync(pokemonOwnedCollection);
    }
    public void InsertAllPokemons()
    {
        Database.DeleteAll(pokemonListCollection);
        for (int index = 0; index < GeneralStats.pokemonList.metas.Length; index++)
        {
            InsertPokemonList((PokemonMeta)GeneralStats.pokemonList.metas[index]);
        }
    }

    public void InsertAllPokemonOwned()
    {
        for (int userIndex = 0; userIndex < GeneralStats.usersData.allUsers.Count; userIndex++)
        {
            for (int pokemonIndex = 0; pokemonIndex < GeneralStats.usersData.allUsers[userIndex].allPokemonData.allPokemons.Length; pokemonIndex++)
            {
                if (GeneralStats.usersData.allUsers[userIndex].allPokemonData.allPokemons[pokemonIndex].isUnlocked)
                {
                    InsertPokemonList(GeneralStats.usersData.allUsers[userIndex].allPokemonData.allPokemons[pokemonIndex].pokemonConfig);
                }
            }
        }
    }
    public void InserPokemonAsync(int _id)
    {
        InsertPokemonList((PokemonMeta)GeneralStats.pokemonList.metas[_id]);
    }
    public void InsertPokemonList(PokemonMeta _meta)
    {
        BsonDocument pokemonToInsert = new BsonDocument { { "_name", _meta.pokemonName }, 
                                                          {"_pokeId", _meta.id.ToString() },
                                                          {"_pokeImg", "" },
                                                          { "_status", true}, 
                                                          { "_type1", ((int)_meta.types[0]).ToString()}, 
                                                          { "_type2", (_meta.types.Length > 1)?((int)_meta.types[1]).ToString(): ""} };

        Database.InsertDocumentAsync(pokemonToInsert, pokemonListCollection);
    }

    public void InsertNewPokemon(string _userId, PokemonData _pokemonToUpdate)
    {
        FilterDefinition<BsonDocument> filter = Builders<BsonDocument>.Filter.Eq("_userId", _userId);
        UpdateDefinition<BsonDocument> update = Builders<BsonDocument>.Update.Set("_pokemonId", _pokemonToUpdate.pokemonIndex.ToString())
                                                                             .Set("_userId", _userId)
                                                                             .Set("_attack", _pokemonToUpdate.GetCurrentAttack().ToString())
                                                                             .Set("_defense", _pokemonToUpdate.GetCurrentDefense().ToString())
                                                                             .Set("_hp", _pokemonToUpdate.GetCurrentLife().ToString())
                                                                             .Set("_level", _pokemonToUpdate.GetCurrentLevel().ToString())
                                                                             .Set("_speed", _pokemonToUpdate.GetCurrentSpeed().ToString())
                                                                             .Set("_active", false);
        Database.UpsertAsync(pokemonOwnedCollection, filter, update);
    }
}
