
using MongoDB.Driver;
using MongoDB.Bson;

public static class Database
{
    public static async void InsertDocumentAsync(BsonDocument _documentToInsert, IMongoCollection<BsonDocument> _collectionToInsert)
    {
        await _collectionToInsert.InsertOneAsync(_documentToInsert);
    }

    public static async void DeleteAllAsync(IMongoCollection<BsonDocument> _collectionToDelete)
    {
        await _collectionToDelete.DeleteManyAsync(new BsonDocument());
    }

    public static void DeleteAll(IMongoCollection<BsonDocument> _collectionToDelete)
    {
        _collectionToDelete.DeleteMany(new BsonDocument());
    }

    public static async void UpsertAsync(IMongoCollection<BsonDocument> _collectionToDelete, FilterDefinition<BsonDocument> _filter, UpdateDefinition<BsonDocument> _update)
    {
        UpdateOptions options = new UpdateOptions { IsUpsert = true };
        await _collectionToDelete.UpdateManyAsync(_filter, _update, options);
    }
}
